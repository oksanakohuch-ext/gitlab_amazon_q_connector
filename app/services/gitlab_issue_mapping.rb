class GitlabIssueMapping
  def initialize(issues)
    @issues = issues
  end

  def documents
    @issues.map do |issue|
      {
        id: issue.id.to_s,
        content: {
          blob: document_data(issue).to_json,
        },
        content_type: 'JSON'
      }
    end
  end

  def document_data(issue)
    {
      document_body: issue.description,
      document_title: issue.title,
      labels: issue.labels.join(', '),
      created_at: issue.created_at.to_s,
      source_uri: issue.web_url,
    }
  end
end