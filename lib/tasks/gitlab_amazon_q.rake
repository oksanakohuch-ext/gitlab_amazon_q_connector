namespace :gitlab_amazon_q do
  desc "Sync GitLab issues with Amazon Q application"
  task sync: :environment do
    # Instantiate the client
    amazon_q = Aws::QBusiness::Client.new

    # Get issues of GitLab project
    gitlab_issues = Gitlab.issues(ENV['GITLAB_PROJECT_ID'])

    # Mapping GitLab issues to an Amazon Q document
    issues_documents = GitlabIssueMapping.new(gitlab_issues).documents

    # Start data source sync job
      response = amazon_q.start_data_source_sync_job(
        application_id: ENV['AMAZON_Q_APP_ID'],
        index_id: ENV['AMAZON_Q_INDEX_ID'],
        data_source_id: ENV['AMAZON_Q_DATA_SOURCE_ID']
      )
    # rescue Aws::QBusiness::Errors::ValidationException

    # Send documents to data source sync job
    amazon_q.batch_put_document(
      application_id: ENV['AMAZON_Q_APP_ID'],
      index_id: ENV['AMAZON_Q_INDEX_ID'],
      data_source_sync_id: response.execution_id,
      documents: issues_documents # issues_documents.merge(repositories_documents)...
    )

    # Stop data source sync job
    amazon_q.stop_data_source_sync_job(
      application_id: ENV['AMAZON_Q_APP_ID'],
      index_id: ENV['AMAZON_Q_INDEX_ID'],
      data_source_id: ENV['AMAZON_Q_DATA_SOURCE_ID']
    )
  end
end
