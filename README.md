# README

1. Create an Amazon Q application with a Custom data source connector following these steps https://docs.aws.amazon.com/amazonq/latest/qbusiness-ug/create-app.html

1. Setup AWS environment variables:

```
# amazon access key
AWS_REGION
AWS_ACCESS_KEY_ID
AWS_SECRET_ACCESS_KEY

# amazon q application details
AMAZON_Q_APP_ID
AMAZON_Q_INDEX_ID
AMAZON_Q_DATA_SOURCE_ID
```

1. Setup GitLab environment variables

```
GITLAB_API_ENDPOINT # https://gitlab.com/api/v4
GITLAB_API_PRIVATE_TOKEN
GITLAB_PROJECT_ID
```

1. Run a rake task `bundle exec rake gitlab_amazon_q:sync` that will sync the data with you Amazon Q application.
